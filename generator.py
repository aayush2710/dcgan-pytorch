#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 21 19:19:33 2020

@author: aayush
"""
import argparse
import torchvision
import torch
import torch.nn as nn
import torch.nn.functional as F
import torchvision.transforms as transforms
import torch.optim as optim
import numpy as np
import matplotlib.pyplot as plt
from torchvision.utils import save_image

class Generator(nn.Module):
    def __init__(self):
        super(Generator, self).__init__()
        self.Project = nn.Sequential(nn.Linear(nz,ngf*16*8*8),
                                     nn.LeakyReLU(0.2,inplace=True)
                                     )
        
        self.main = nn.Sequential(
        
            nn.ConvTranspose2d(ngf * 16, ngf * 8, 5,stride=2, padding=1, output_padding=0),
            nn.BatchNorm2d(ngf * 8),
            nn.LeakyReLU(0.2, inplace=True),
            
            nn.ConvTranspose2d( ngf * 8, ngf * 4, 5, stride=2, padding=2, output_padding=0),
            nn.BatchNorm2d(ngf * 4),
            nn.LeakyReLU(0.2, inplace=True),
            
            nn.ConvTranspose2d( ngf * 4, ngf*2, 5, stride=2, padding=2, output_padding=0),
            nn.BatchNorm2d(ngf*2),
            nn.LeakyReLU(0.2, inplace=True),
            
            nn.ConvTranspose2d( ngf * 2, ngf, 5, stride=2, padding=2, output_padding=1),
            nn.BatchNorm2d(ngf),
            nn.LeakyReLU(0.2, inplace=True),
            
            nn.Conv2d( ngf, nc, 5,stride=1, padding=1),
            nn.Tanh()
        )
    def forward(self, input):
        return self.main(self.Project(input).view(-1,ngf*16,8,8))


class Discriminator(nn.Module):
    def __init__(self):
        super(Discriminator, self).__init__()
        self.main = nn.Sequential(
            # input is (nc) x 64 x 64
            nn.Conv2d(nc, ndf, 5, 2, 1),
            nn.BatchNorm2d(ndf),
            nn.LeakyReLU(0.2, inplace=True),
            # state size. (ndf) x 32 x 32
            nn.Conv2d(ndf, ndf * 2, 5, 2, 1),
            nn.BatchNorm2d(ndf * 2),
            nn.LeakyReLU(0.2, inplace=True),
            # state size. (ndf*2) x 16 x 16
            nn.Conv2d(ndf * 2, ndf * 4, 5, 2, 1),
            nn.BatchNorm2d(ndf * 4),
            nn.LeakyReLU(0.2, inplace=True),
            # state size. (ndf*4) x 8 x 8
            nn.Conv2d(ndf * 4, ndf * 8, 5, 1, 2),
            nn.BatchNorm2d(ndf * 8),
            nn.LeakyReLU(0.2, inplace=True),
            # state size. (ndf*8) x 4 x 4
            nn.Conv2d(ndf * 8, ndf*16, 5, 2, 2),
            nn.BatchNorm2d(ndf * 16),
            nn.LeakyReLU(0.2, inplace=True))
        
        self.transform = nn.Sequential(
            nn.Linear(8*8*ndf*16,1),
            nn.Sigmoid()
        )

    def forward(self, input):
        return self.transform(self.main(input).view(-1,8*8*ndf*16))


image_size = 128
nc = 3
nz = 100
ngf = 64
ndf = 64

parser = argparse.ArgumentParser()
parser.add_argument('--weights', required=True ,help='weights')
parser.add_argument('--output', default="image.png", help='Output image name')

opt = parser.parse_args()


Gnet = Generator()
Gnet.load_state_dict(torch.load(opt.weights,map_location=torch.device('cpu') )) 
rand = torch.randn(1, nz, device=torch.device('cpu'))
output = Gnet(rand)[0].detach()
save_image(output,open(opt.output , "wb"))
