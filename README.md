# Cartoon-Generator-DCGAN-Pytorch


--> To train the model run the following command in the repo directory
``` 
python train.py --data_path "simpsons-faces" --batch 64 --epochs 350  --log_path "log"

```

--> To generate output run
``` 
python generate.py --weights G_net.pt --output image1.png

```

--> To see all parameters
``` 
python train.py --h

```
``` 
python generate.py --h

```

# Transformation during training

![](training.gif)

# Some generated samples

![](Generated%20Samples/generated1.png)         ![](Generated%20Samples/generated2.png)     ![](Generated%20Samples/generated3.png)     ![](Generated%20Samples/generated4.png)  ![](Generated%20Samples/generated5.png)

![](Generated%20Samples/generated6.png)         ![](Generated%20Samples/generated7.png)     ![](Generated%20Samples/generated8.png)     ![](Generated%20Samples/generated9.png)  ![](Generated%20Samples/generated10.png)

![](Generated%20Samples/generated11.png)         ![](Generated%20Samples/generated12.png)     ![](Generated%20Samples/generated13.png)     ![](Generated%20Samples/generated14.png)  ![](Generated%20Samples/generated15.png)

![](Generated%20Samples/generated16.png)         ![](Generated%20Samples/generated17.png)     ![](Generated%20Samples/generated18.png)     ![](Generated%20Samples/generated19.png)  ![](Generated%20Samples/generated20.png)



