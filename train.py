#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 19 18:40:20 2020

@author: aayush
"""
import os
import numpy as np
import argparse
import torchvision
import torch
import torch.nn as nn
import torch.nn.functional as F
import torchvision.transforms as transforms
import torch.optim as optim
import os

from torchvision.utils import save_image
import matplotlib.pyplot as plt
def image_loader(path , shape, batch_size):
    loader = torchvision.datasets.ImageFolder(root = path , transform=transforms.Compose([
                                   transforms.Resize(shape),
                                   transforms.ToTensor(),
                                   transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))]))
    
    loader = torch.utils.data.DataLoader(loader, batch_size=batch_size,
                                         shuffle=True,num_workers=8)
    print(len(loader))
    
    return loader
    

class Generator(nn.Module):
    def __init__(self):
        super(Generator, self).__init__()
        self.Project = nn.Sequential(nn.Linear(nz,ngf*16*8*8),
                                     nn.LeakyReLU(0.2,inplace=True)
                                     )
        
        self.main = nn.Sequential(
        
            nn.ConvTranspose2d(ngf * 16, ngf * 8, 5,stride=2, padding=1, output_padding=0),
            nn.BatchNorm2d(ngf * 8),
            nn.LeakyReLU(0.2, inplace=True),
            
            nn.ConvTranspose2d( ngf * 8, ngf * 4, 5, stride=2, padding=2, output_padding=0),
            nn.BatchNorm2d(ngf * 4),
            nn.LeakyReLU(0.2, inplace=True),
            
            nn.ConvTranspose2d( ngf * 4, ngf*2, 5, stride=2, padding=2, output_padding=0),
            nn.BatchNorm2d(ngf*2),
            nn.LeakyReLU(0.2, inplace=True),
            
            nn.ConvTranspose2d( ngf * 2, ngf, 5, stride=2, padding=2, output_padding=1),
            nn.BatchNorm2d(ngf),
            nn.LeakyReLU(0.2, inplace=True),
            
            nn.Conv2d( ngf, nc, 5,stride=1, padding=1),
            nn.Tanh()
        )
    def forward(self, input):
        return self.main(self.Project(input).view(-1,ngf*16,8,8))


class Discriminator(nn.Module):
    def __init__(self):
        super(Discriminator, self).__init__()
        self.main = nn.Sequential(
            # input is (nc) x 64 x 64
            nn.Conv2d(nc, ndf, 5, 2, 1),
            nn.BatchNorm2d(ndf),
            nn.LeakyReLU(0.2, inplace=True),
            # state size. (ndf) x 32 x 32
            nn.Conv2d(ndf, ndf * 2, 5, 2, 1),
            nn.BatchNorm2d(ndf * 2),
            nn.LeakyReLU(0.2, inplace=True),
            # state size. (ndf*2) x 16 x 16
            nn.Conv2d(ndf * 2, ndf * 4, 5, 2, 1),
            nn.BatchNorm2d(ndf * 4),
            nn.LeakyReLU(0.2, inplace=True),
            # state size. (ndf*4) x 8 x 8
            nn.Conv2d(ndf * 4, ndf * 8, 5, 1, 2),
            nn.BatchNorm2d(ndf * 8),
            nn.LeakyReLU(0.2, inplace=True),
            # state size. (ndf*8) x 4 x 4
            nn.Conv2d(ndf * 8, ndf*16, 5, 2, 2),
            nn.BatchNorm2d(ndf * 16),
            nn.LeakyReLU(0.2, inplace=True))
        
        self.transform = nn.Sequential(
            nn.Linear(8*8*ndf*16,1),
            nn.Sigmoid()
        )

    def forward(self, input):
        return self.transform(self.main(input).view(-1,8*8*ndf*16))


def weights_init(m):
    classname = m.__class__.__name__
    if classname.find('Conv') != -1:
        nn.init.normal_(m.weight.data, 0.0, 0.02)
    elif classname.find('BatchNorm') != -1:
        nn.init.normal_(m.weight.data, 1.0, 0.02)
        nn.init.constant_(m.bias.data, 0)
        
        
def loss_optimizer(lrG,lrD,opt = "adam"):
    loss = nn.BCELoss()
    if opt.lower() =="sgd":
        optimD = optim.SGD(D_net.parameters(), lr=lrD, betas=(beta1, 0.999))
        optimG = optim.SGD(G_net.parameters(), lr=lrG, betas=(beta1, 0.999))
    elif opt.lower() == "adam":
        optimD = optim.Adam(D_net.parameters(), lr=lrD, betas=(beta1, 0.999))
        optimG = optim.Adam(G_net.parameters(), lr=lrG, betas=(beta1, 0.999))
        
    return loss,optimD,optimG

def train(trainloader,epoch,loss,optD,optG,outpath):
    for _ in range(1,epoch+1):
        for i,data in enumerate(trainloader):
            D_net.zero_grad()
            real = data[0].to(device)
            b_size = len(data[0])
            labels_fake = (((torch.rand(b_size)+1)/2)+0.2).to(device, dtype=torch.float) # labels  0.7 - 1.2   
            labels_real = ((torch.rand(b_size))/3).to(device, dtype=torch.float)  # labels  0 - 0.3
    
            rnd_indexes = set(np.random.randint(b_size, size=int(0.05*b_size)))
        
            # flip the labels of 5% of the data for disciminator only
            for i in rnd_indexes:
                labels_real[i] = ((labels_real[i]+1)/2)+0.2
                labels_fake[i] = labels_fake[i]/3 - 0.1
            
            
            output = D_net(real).view(-1)
            D_x = output.mean().item()
            ReallossD = loss(output,labels_real)
            ReallossD.backward()
            D_x = output.mean().item()
            rand = torch.randn(real.shape[0], nz, device=device)
            generated = G_net(rand)
       
            output = D_net(generated.detach()).view(-1)
            generatedlossD = loss(output,labels_fake)
            generatedlossD.backward()
            D_G_z1 = output.mean().item()
            D_Loss = ReallossD + generatedlossD
            
            optD.step()
            
            labels_real = (torch.zeros(b_size)).to(device, dtype=torch.float) 
            G_net.zero_grad()
            
            output = D_net(generated).view(-1)
            GLoss = loss(output,labels_real)
            GLoss.backward()
            D_G_z2 = output.mean().item()
            optG.step()
            
        print('[%d/%d][%d/%d]\tLoss_D: %.4f\tLoss_G: %.4f\tD(x): %.4f\tD(G(z)): %.4f / %.4f'
                  % (_, epoch, len(trainloader), len(trainloader),
                     D_Loss.item(), GLoss.item(), D_x, D_G_z1, D_G_z2))
        
        f = open(outpath+"log.txt" , "a")
        f.write('[%d/%d][%d/%d]\tLoss_D: %.4f\tLoss_G: %.4f\tD(x): %.4f\tD(G(z)): %.4f / %.4f'
                  % (_, epoch, len(trainloader), len(trainloader),
                     D_Loss.item(), GLoss.item(), D_x, D_G_z1, D_G_z2))
        f.close()
        torch.save(D_net.state_dict(), outpath+"D_net"+str(_)+".pt")
        torch.save(G_net.state_dict(), outpath+"G_net"+str(_)+".pt")
        if _%10 == 0:
          save_image(G_net(torch.randn(real.shape[0], nz, device=device))[0].detach(),open(str(_)+".png" , "wb"))
            
            
            
            
            
            
            
parser = argparse.ArgumentParser()
parser.add_argument('--data_path', required=False, default = "simpsons-faces" ,help='path to dataset')
parser.add_argument('--workers', type=int, help='number of data loading workers', default=2)
parser.add_argument('--k', type=int, default=1, help='GAN ratio')
parser.add_argument('--lrGen', type=float, default=0.0004, help='learning rate, default=0.0004')
parser.add_argument('--lrDis', type=float, default=0.00004, help='learning rate, default=0.00004')
parser.add_argument('--cuda', action='store_true', default = True ,help='enables cuda')
parser.add_argument('--batch', type=int, default=32, help='BatchSize')
parser.add_argument('--epochs', type=int, default=100, help='Epochs')
parser.add_argument('--log_path' , default="log", help='Output Path')
parser.add_argument('--testPath' , default="log", help='Test Images PAth')
opt = parser.parse_args()
batch_size = opt.batch
image_size = 128
nc = 3
nz = 100
ngf = 64
ndf = 64
num_epochs = 5
lrG = opt.lrGen
lrD = opt.lrDis
beta1 = 0.5
real_label = 1
fake_label = 0
outpath = os.getcwd() + opt.log_path
epochs = opt.epochs
if not os.path.isdir(outpath):
            os.mkdir(outpath)
device = torch.device("cuda:0" if (torch.cuda.is_available() and opt.cuda) else "cpu")
print("Training on:" , device)
trainloader = image_loader(opt.data_path, (image_size,image_size), batch_size)
D_net = Discriminator().to(device)
G_net = Generator().to(device)
D_net.apply(weights_init)
G_net.apply(weights_init)
loss,optD,optG = loss_optimizer(lrG,lrD)
train(trainloader , epochs , loss , optD, optG,outpath)




 